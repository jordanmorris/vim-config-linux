set guioptions-=a

"Ruby/rails stuff BEGIN
if $PATH !~ "\.rbenv"
	let $PATH="/home/jordanmorris/.rbenv/shims:/home/jordanmorris/.rbenv/bin:" . $PATH
endif
command -bar -nargs=1 OpenURL :!/usr/bin/google-chrome-stable <args>
"Ruby/rails stuff END

if has("gui_running")
  " GUI is running or is about to start.
  " Maximize gvim window (for an alternative on Windows, see simalt below).
  set lines=999 columns=999
else
  " This is console Vim.
  if exists("+lines")
    set lines=50
  endif
  if exists("+columns")
    set columns=100
  endif
endif
